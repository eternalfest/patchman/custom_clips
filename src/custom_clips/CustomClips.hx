package custom_clips;

import etwin.Error;
import etwin.flash.MovieClip;
import hf.Hf;
import patchman.Ref;
import patchman.IPatch;
import patchman.PatchList;

/**
  This mod provides greater control over associations between `MovieClip` and classes made
  with `hf.Std.registerClass`.

  **WARNING**: If any registrations are made using `Object.registerClass` directly, or
  using `hf.Std.registerClass` before this mod's patches are applied, the methods of this
  class will not work correctly and may corrupt the registered mappings.
**/
@:build(patchman.Build.di())
class CustomClips {
  private static var UID: Int = 0;
  private static var REGISTERED_CLASSES(default, null): Map<String, Dynamic> = new Map();
  private static var LINK_SUBSTITUTIONS: Map<String, Null<String>> = new Map();

  private static var PATCH: IPatch = new PatchList([
    Ref.auto(hf.Std.registerClass).replace(function(hf, symbol, cls) {
      var cls: Class<MovieClip> = cast cls;
      doRegisterClass(REGISTERED_CLASSES, symbol, cls);
      return true;
    }),
    Ref.auto(hf.Std.attachMC).replace(function(hf, parent, symbol, depth) {
      symbol = maybeSubstituteSymbol(symbol);
      return parent.attachMovie(symbol, '$symbol@custom@${UID++}', depth);
    }),
  ]);

  @:diExport
  public var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = PATCH;
  }

  /**
    Associate a `MovieClip` symbol with the given class.

    This is an alias for `hf.Std.registerClass()`.
  **/
  public inline function registerClass<T: MovieClip>(symbol: String, cls: Null<Class<T>>): Void {
    doRegisterClass(REGISTERED_CLASSES, symbol, cls);
  }

  /**
    Returns the class currently associated with the given `MovieClip` symbol, or
    `null` if no such class exists.
  **/
  public inline function getRegisteredClass(symbol: String): Null<Class<MovieClip>> {
    return REGISTERED_CLASSES.get(symbol);
  }

  /**
    Takes a `MovieClip` symbol and attaches it to the `parent` clip, making it an
    instance of the given class.

    If `depth` is omitted, the clip will be created in front of all other clips.
  **/
  public function addMovie<T: MovieClip>(parent: MovieClip, symbol: String, cls: Null<Class<T>>, ?depth: Int): T {
    symbol = maybeSubstituteSymbol(symbol);
    depth = depth == null ? parent.getNextHighestDepth() : depth;
    var registered = REGISTERED_CLASSES;
    var prevCls = registered.get(symbol);

    if (prevCls == cls) {
      return cast parent.attachMovie(symbol, '$symbol@custom@${UID++}', depth);
    } else {
      doRegisterClass(registered, symbol, cls);
      var result = parent.attachMovie(symbol, '$symbol@custom@${UID++}', depth);
      doRegisterClass(registered, symbol, prevCls);
      return cast result;
    }
  }

  /**
    Takes a `MovieClip` symbol and attaches it to the given `DepthManager`, making it an
    instance of the given class.
  **/
  public function attach<T: MovieClip>(depthMan: hf.DepthManager, symbol: String, plan: Int, cls: Class<T>): T {
    var substitued = maybeSubstituteSymbol(symbol);
    var registered = REGISTERED_CLASSES;
    var prevCls = registered.get(substitued);

    if (prevCls == cls) {
      // The hf.Std.attachMovie patch will substitute the symbol
      return cast depthMan.attach(symbol, plan);
    } else {
      doRegisterClass(registered, substitued, cls);
      // The hf.Std.attachMovie patch will substitute the symbol
      var result = depthMan.attach(symbol, plan);  
      doRegisterClass(registered, substitued, prevCls);
      return cast result;
    }
  }

  /**
    Create a "substitution scope" in which to execute the given function.
    
    Inside this scope, all `MovieClip`s created with `hf.Std.attachMovie(symbol, ...)`
    will use `replacement` instead, but will keep the class associated with `symbol`.
    These scopes can be nested, allowing for several substitutions at once.
  **/
  public function substitute<T>(symbol: String, replacement: String, func: Void -> T): T {
    if (symbol == replacement) {
      return func(); // No-op
    }

    var substitutions = LINK_SUBSTITUTIONS;
    var registered = REGISTERED_CLASSES;

    var prevReplacement = substitutions.get(symbol);
    var prevCls = registered.get(replacement);
    substitutions.set(symbol, replacement);
    doRegisterClass(registered, replacement, registered.get(symbol));

    var result = func();

    substitutions.set(symbol, prevReplacement);
    doRegisterClass(registered, replacement, prevCls);
    return result;
  }

  private static inline function maybeSubstituteSymbol(symbol: String): String {
    var substitued = LINK_SUBSTITUTIONS.get(symbol);
    return substitued == null ? symbol : substitued;
  }

  private static inline function doRegisterClass<T: MovieClip>(registeredClasses: Map<String, Dynamic>, symbol: String, cls: Null<Class<T>>) {
    if (!(untyped __eval__("Object").registerClass(symbol, cls))) {
      throw new Error("RegisterClassError: " + symbol);
    }
    registeredClasses.set(symbol, cls);
  }
}
